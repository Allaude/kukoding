<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Tes extends MX_Controller {

    public function index()
    {
        echo modules::run('admin/dashboard/tes');
    }

    public function coba()
    {
        $this->page->view('penggajian_form', array(
			'action'	 	=> $this->page->base_url("/insert_transaksi_gaji"),
		));
    }
}
