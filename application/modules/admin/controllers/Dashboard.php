<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
        $this->load->library(['ion_auth']);
        
        if (!$this->ion_auth->is_admin())
        {
          $this->session->set_flashdata('message', 'You must be an admin to view this page');
          redirect('/');
        }
        
	}

    public function index()
    {
        $this->load->view('header');
        $this->load->view('aside');
        $this->load->view('dashboard');
        $this->load->view('footer');
    }
}
